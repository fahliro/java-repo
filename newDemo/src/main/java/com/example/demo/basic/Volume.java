package com.example.demo.basic;

public class Volume extends Luas {
    private Integer tinggi;
    private Luas luas = new Luas();

    public Volume(Integer panjang, Integer lebar, Integer tinggi) {
        luas.setPanjang(panjang);
        luas.setLebar(lebar);
        this.tinggi=tinggi;
    }

    public Integer volumePersegi() {
        Integer hasil=luas.getPanjang()*luas.getLebar()*tinggi;
        return hasil;
    }

    public Volume() {

    }

    public Integer getTinggi() {
        return tinggi;
    }

    public void setTinggi(Integer tinggi) {
        this.tinggi = tinggi;
    }

}
