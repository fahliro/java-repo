package com.example.demo.services;

import com.example.demo.models.Book;
import com.example.demo.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Service
@Transactional
public class BookService {

    private EntityManagerFactory emf;

    @Autowired
    public void setEmf(EntityManagerFactory emf) {this.emf = emf;}

    @Autowired
    BookRepository bookRepository;

    public List<Book> findAll() {
        return bookRepository.findAll();
    }


    public Book simpanEdit(Book book) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Book tersimpan = em.merge(book);
        em.getTransaction().commit();
        return tersimpan;
    }

    public Book getId(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Book.class, id);
    }

    public void hapus(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Book.class, id));
        em.getTransaction().commit();
    }

}
