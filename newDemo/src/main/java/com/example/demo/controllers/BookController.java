package com.example.demo.controllers;

import com.example.demo.models.Book;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping(value = {"","/"})
    public String listBook(Model model) {
        model.addAttribute("Books",bookService.findAll());
        return "books/list";
    }


    @RequestMapping(value= {"/create"}, method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("Books", new Book());
        return "books/create";
    }

    @RequestMapping(value= {"/create"}, method = RequestMethod.POST)
    public String create_action(Model model, Book book) {
        model.addAttribute("Books", bookService.simpanEdit(book));
        return "redirect:/book";
    }

    @RequestMapping(value= {"/edit/{id}"}, method = RequestMethod.GET)
    public String edit_data(@PathVariable Integer id, Model model) {
        model.addAttribute("Books", bookService.getId(id));
        return "books/create";
    }

    @RequestMapping(value= {"/delete/{id}"})
    public String hapus(@PathVariable Integer id) {
        bookService.hapus(id);
        return "redirect:/book";
    }
}
